![img](https://i.ytimg.com/vi/X6jEjbSFE7c/maxresdefault.jpg?v=5ec72e6b)
# Newangers
WIP game on Unigine engine.
Dev channels:

[youtube.com/channel/UC_BTcoKrou9mk5txW0PcdqQ](https://www.youtube.com/channel/UC_BTcoKrou9mk5txW0PcdqQ)

[youtube.com/He6oru](https://youtube.com/He6oru)

# Install
Clone repo
https://gitlab.com/axelaredz/newangers.git


# Setup on Windows
Rename dir and files 
bin_windows and newangers.project_windows
to
bin and newangers.project

Edit string 10104 in file newangers.project
"path": "PATH_TO/UNIGINE_SDK_Browser/sdks/community_windows_2.11.0.2/",


# Setup on LINUX
Rename dir and files 
bin_linux and newangers.project_linux
to
bin and newangers.project

Edit string 10104 in file newangers.project
"path": "/home/USER/PATH_TO/UNIGINE_SDK_Browser/sdks/community_linux_2.11.0.2/",


# Import project
Push button ADD EXISTING and browse project to path

# Control
WSAD - move

QE - left\right strafe

ZX - jump and down impulse

YUIO - select mechos

JN - select wheels

KM - fly mode

L - reset

B - water world

Esc - Exit game