using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "a2d0ee1f00f264f2b8987fa9d68bc4f40dd52bc3")]
public class TheControlCenter : Component
{

	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Chose Heretic")]
	private Input.KEY Chose1Key = Input.KEY.Y;
	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Chose Weaver")]
	private Input.KEY Chose2Key = Input.KEY.U;
	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Chose Om")]
	private Input.KEY Chose3Key = Input.KEY.I;
	[ShowInEditor][Parameter(Group = "Control", Tooltip = "CnangeState")]
	private Input.KEY changeKey = Input.KEY.K;

	[ShowInEditor][Parameter(Tooltip = "theHeretic")]
	private Node targetHeretic = null;
	[ShowInEditor][Parameter(Tooltip = "theWeaver")]
	private Node targetWeaver = null;
	[ShowInEditor][Parameter(Tooltip = "theOm")]
	private Node targetOm = null;

	[ShowInEditor][Parameter(Tooltip = "theGhost")]
	private Node targetGhost = null;

	[ShowInEditor][Parameter(Tooltip = "Left Wheel")]
	private Node targetWheelL = null;
	[ShowInEditor][Parameter(Tooltip = "Right Wheel")]
	private Node targetWheelR = null;
    private JointWheel my_jointL;
    private JointWheel my_jointR;

	[ShowInEditor][Parameter(Tooltip = "Left Wheel Forward")]
	private Node targetWheelFL = null;
	[ShowInEditor][Parameter(Tooltip = "Right Wheel Forward")]
	private Node targetWheelFR = null;
    private JointWheel my_jointFL;
    private JointWheel my_jointFR;

	[ShowInEditor][Parameter(Tooltip = "Left Wheel Body")]
	private Node targetWBL = null;
	[ShowInEditor][Parameter(Tooltip = "Right Wheel Body")]
	private Node targetWBR = null;

	[ShowInEditor][ParameterFile(Group = "Effects", Tooltip = "Asset file that contains explosion effect")]
	private string explosionEffectReference = null;
	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains the spawn effect")]
	private string spawnEffectReference = "";

	private float myTimer = 0.0f;
	private int mechosState = 1;//мехос является мехосом

	[ShowInEditor][Parameter(Tooltip = "Anomal Gravitation")]
	private Node targetAnomalG = null;

	[ShowInEditor][Parameter(Tooltip = "land1")]
	private Node target1= null;
	[ShowInEditor][Parameter(Tooltip = "water")]
	private Node target2= null;
	[ShowInEditor][Parameter(Tooltip = "land2")]
	private Node target3= null;

	private void Init()
	{
	my_jointL = targetWheelL.ObjectBodyRigid.GetJoint(0) as JointWheel;
	my_jointR = targetWheelR.ObjectBodyRigid.GetJoint(0) as JointWheel;
	my_jointFL = targetWheelFL.ObjectBodyRigid.GetJoint(0) as JointWheel;
	my_jointFR = targetWheelFR.ObjectBodyRigid.GetJoint(0) as JointWheel;
	}
	
	private void Update()
	{

			if (Input.IsKeyPressed(Chose1Key))//выбрать еретика
			{
              targetHeretic.Enabled = true;
			  targetWeaver.Enabled = false;
			  targetOm.Enabled = false;
			  targetGhost.Enabled = false;
			  my_jointL.Anchor0 = new vec3(-1.5f,-1.8f,-0.2f);
			  my_jointR.Anchor0 = new vec3(1.5f,-1.8f,-0.2f);
			  my_jointL.WheelRadius = 1.8f;
			  my_jointR.WheelRadius = 1.8f;
			  my_jointFL.Anchor0 = new vec3(-1f,2f,-0.5f);
			  my_jointFR.Anchor0 = new vec3(1f,2f,-0.5f);
	          targetWBL.Scale = new vec3(54f,54f,54f);
		      targetWBR.Scale = new vec3(54f,54f,54f);

			  my_jointL.LinearDamping = 100f;// изначально было 20
			  my_jointR.LinearDamping = 100f;
			  my_jointFL.LinearDamping = 100f;
			  my_jointFR.LinearDamping = 100f;
			}
			if (Input.IsKeyPressed(Chose2Key))//выбрать ткача
			{
              targetHeretic.Enabled = false;
			  targetWeaver.Enabled = true;
			  targetOm.Enabled = false;
			  targetGhost.Enabled = false;
			  my_jointL.Anchor0 = new vec3(-1.5f,-1.8f,-0.2f);
			  my_jointR.Anchor0 = new vec3(1.5f,-1.8f,-0.2f);
			  my_jointL.WheelRadius = 1.4f;
			  my_jointR.WheelRadius = 1.4f;
			  my_jointFL.Anchor0 = new vec3(-1.5f,2f,-0.5f);
			  my_jointFR.Anchor0 = new vec3(1.5f,2f,-0.5f);
	          targetWBL.Scale = new vec3(40f,40f,40f);
		      targetWBR.Scale = new vec3(40f,40f,40f);

			  my_jointL.LinearDamping = 90f;
			  my_jointR.LinearDamping = 90f;
			  my_jointFL.LinearDamping = 90f;
			  my_jointFR.LinearDamping = 90f;
			}
			if (Input.IsKeyPressed(Chose3Key))//выбрать Oма
			{
			  targetHeretic.Enabled = false;
			  targetWeaver.Enabled = false;
			  targetOm.Enabled = true;
			  targetGhost.Enabled = false;
			  my_jointL.Anchor0 = new vec3(-2.7f,-1.8f,-0.2f);
			  my_jointR.Anchor0 = new vec3(2.7f,-1.8f,-0.2f);
			  my_jointL.WheelRadius = 1.4f;
			  my_jointR.WheelRadius = 1.4f;
			  my_jointFL.Anchor0 = new vec3(-2.5f,3.9f,-0.5f);
			  my_jointFR.Anchor0 = new vec3(2.5f,3.9f,-0.5f);
	          targetWBL.Scale = new vec3(40f,40f,40f);
		      targetWBR.Scale = new vec3(40f,40f,40f);

			  my_jointL.LinearDamping = 25f;
			  my_jointR.LinearDamping = 25f;
			  my_jointFL.LinearDamping = 25f;
			  my_jointFR.LinearDamping = 25f;
			}

			if (Input.IsKeyPressed(Input.KEY.O))//выбрать призрака
			{
              targetHeretic.Enabled = false;
			  targetWeaver.Enabled = false;
			  targetOm.Enabled = false;
			  targetGhost.Enabled = true;
			  my_jointL.Anchor0 = new vec3(-1.9f,-1.8f,-0.2f);
			  my_jointR.Anchor0 = new vec3(1.9f,-1.8f,-0.2f);
			  my_jointL.WheelRadius = 1.4f;
			  my_jointR.WheelRadius = 1.4f;
			  my_jointFL.Anchor0 = new vec3(-1.9f,2f,-0.5f);
			  my_jointFR.Anchor0 = new vec3(1.9f,2f,-0.5f);
	          targetWBL.Scale = new vec3(40f,40f,40f);
		      targetWBR.Scale = new vec3(40f,40f,40f);

			  my_jointL.LinearDamping = 70f;
			  my_jointR.LinearDamping = 70f;
			  my_jointFL.LinearDamping = 70f;
			  my_jointFR.LinearDamping = 70f;
			}

			if (Input.IsKeyDown(Input.KEY.ESC))//ВЫХОД ИЗ ПРИЛОЖЕНИЯ ПО КНОПКЕ
			{
				App.Exit();
			}
			if (Input.IsKeyDown(Input.KEY.L))//взрыв мехоса
			{
				DeadEvent();
			}

			if (Input.IsKeyPressed(changeKey)&&mechosState==1)//сменить на летающий кораблик
			{
               mechosState=0;
				   targetWheelL.Enabled=false;
				   targetWheelR.Enabled=false;
				   targetWheelFL.Enabled=false;
				   targetWheelFR.Enabled=false;
				   targetAnomalG.Enabled=true;

				   node.ObjectBodyRigid.AngularScale=new vec3(0f,0f,1f);
			}
			if (Input.IsKeyDown(Input.KEY.M)&&mechosState==0)//сменить на мехос, обратно
			{
               mechosState=1;
				   targetWheelL.Enabled=true;
				   targetWheelR.Enabled=true;
				   targetWheelFL.Enabled=true;
				   targetWheelFR.Enabled=true;
				   targetAnomalG.Enabled=false;

				   node.ObjectBodyRigid.AngularScale=new vec3(1f,1f,1f);
			}

			if (Input.IsKeyDown(Input.KEY.W)&&mechosState==0)//импульс вперёд вне мехоса
			{
				node.ObjectBodyRigid.AngularVelocity=new vec3(0f,0f,0f);
				node.ObjectBodyRigid.AddLinearImpulse(node.GetWorldDirection(MathLib.AXIS.Y) * 1300f);
			}

			if (Input.IsKeyDown(Input.KEY.J))//тест поворота осей
			{
			  //my_jointL.Axis10 = new vec3(-1f,0f,0f);//в обратную сторону
			  
			  // перестановка колёс в положение боком
			  my_jointL.Axis11 = new vec3(1f,0f,0f);
			  my_jointR.Axis11 = new vec3(1f,0f,0f);
			  my_jointFL.Axis11 = new vec3(1f,0f,0f);
			  my_jointFR.Axis11 = new vec3(1f,0f,0f);

			  //my_jointR.Anchor1 = new vec3(2.7f,-1.8f,-0.2f);
			  //my_jointFL.Anchor1 = new vec3(-2.5f,3.9f,-0.5f);
			  //my_jointFR.Anchor1 = new vec3(2.5f,3.9f,-0.5f);
			}
			if (Input.IsKeyDown(Input.KEY.N))//поворот осей вернуть
			{
			  my_jointL.Axis11 = new vec3(0f,0f,1f);
			  my_jointR.Axis11 = new vec3(0f,0f,1f);
			  my_jointFL.Axis11 = new vec3(0f,0f,1f);
			  my_jointFR.Axis11 = new vec3(0f,0f,1f);
			}
			if (Input.IsKeyDown(Input.KEY.B))//смена мира
			{
              target1.Enabled=false;
			  target2.Enabled=true;
			  target3.Enabled=true;
			}


            // действия в таймере
			myTimer+=1f;
			
			if (myTimer>2f){
			 myTimer = 0f;
			 //SledEvent();
			}
			// конец действий в таймере
		
	}

	private void DeadEvent()
	{
		Node effect = World.LoadNode(explosionEffectReference);
		effect.WorldPosition = node.WorldPosition;

		node.DeleteLater();
		World.LoadWorld("wild_engines");
		//World.LoadWorld("data/game/game_land");
	}

	private void SledEvent()
	{

	    Node spawnEffect = World.LoadNode(spawnEffectReference);
		//spawnEffect.WorldPosition = new vec3(targetWBL.GetWorldDirection(MathLib.AXIS.X),0f,0f);//targetWBL.WorldPosition+vec3.RIGHT*5f;
        spawnEffect.WorldPosition = targetWBL.WorldPosition;

		spawnEffect = spawnEffect.Clone();
		spawnEffect.WorldPosition = targetWBR.WorldPosition;

		//Node spawnEffect = World.LoadNode(spawnEffectReference);
		//spawnEffect.WorldPosition = node.WorldPosition;

		//spawnEffect.DeleteLater();
	}

}
