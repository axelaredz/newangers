using System;
using System.Collections;
using System.Collections.Generic;

using System.Linq;//может быть не нужно

using Unigine;

[Component(PropertyGuid = "a5431d7a3f800e91e7e676bc6ecde2eb0e401b4e")]
public class TheCarDrive : Component
{

	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Forward axis")]
	private Input.KEY forwardKey = Input.KEY.W;
	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Backward axis")]
	private Input.KEY backwardKey = Input.KEY.S;
	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Left axis")]
	private Input.KEY moveLeftKey = Input.KEY.A;
	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Right axis")]
	private Input.KEY moveRightKey = Input.KEY.D;

	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Jump button")]
	private Input.KEY jumpKey = Input.KEY.SPACE;

//	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Left strafe")]
//	private Input.KEY stLeftKey = Input.KEY.Q;
	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Right strafe")]
	private Input.KEY stRightKey = Input.KEY.E;

	[ShowInEditor][Parameter(Tooltip = "Left Wheel")]
	private Node targetWheelL = null;
	[ShowInEditor][Parameter(Tooltip = "Right Wheel")]
	private Node targetWheelR = null;

	[ShowInEditor][Parameter(Tooltip = "theCar")]
	private Node targetCar = null;

//    JointWheel[] wheel_joints = new JointWheel[4];
  private JointWheel my_jointL;// = new JointWheel();
  private JointWheel my_jointR;// = new JointWheel();

 // private BodyRigid my_rig;
	//Controls controls = null;

	private float ifps;

	private void Init()
	{
	//my_jointL= targetWheelL.GetComponent<JointWheel>();
	//my_rig= targetWheelL.GetComponent<BodyRigid>();
	my_jointL = targetWheelL.ObjectBodyRigid.GetJoint(0) as JointWheel;
	//my_jointL.AngularVelocity = 7f;
	//my_jointL.AngularTorque = 7f;

	my_jointR = targetWheelR.ObjectBodyRigid.GetJoint(0) as JointWheel;
	//my_jointR.AngularVelocity = 7f;
	//my_jointR.AngularTorque = 7f;
	}
	
	private void Update()
	{
			ifps = Game.IFps;

			// forward and backward movement by setting joint motor's velocity and torque
//			if ((controls.GetState(Controls.STATE_FORWARD) == 1) )// || (controls.GetState(Controls.STATE_TURN_UP) == 1))
//			{
//				my_jointR.AngularVelocity = MathLib.Max(my_jointR.AngularVelocity, 0.0f);
//				my_jointR.AngularVelocity += ifps * 50.0f;
//				my_jointR.AngularTorque = 5.0f;
//			}

			if (Input.IsKeyPressed(forwardKey))
			{
				my_jointL.AngularVelocity = MathLib.Max(my_jointL.AngularVelocity, 0.0f);
				my_jointL.AngularVelocity += ifps * 50.0f;
				my_jointL.AngularTorque = 5.0f;

				my_jointR.AngularVelocity = MathLib.Max(my_jointR.AngularVelocity, 0.0f);
				my_jointR.AngularVelocity += ifps * 50.0f;
				my_jointR.AngularTorque = 5.0f;

				my_jointL.AngularDamping = 0.0f;
				my_jointR.AngularDamping = 0.0f;
			}
        //    else
		//	{
		//		my_jointR.AngularVelocity = 0.0f;
		//		my_jointR.AngularTorque = 0.0f;
		//		my_jointL.AngularVelocity = 0.0f;
		//		my_jointL.AngularTorque = 0.0f;
		//	}

			if (Input.IsKeyPressed(backwardKey))
			{
				my_jointL.AngularVelocity = MathLib.Max(my_jointL.AngularVelocity, 0.0f);
				my_jointL.AngularVelocity -= ifps * 50.0f;
				my_jointL.AngularTorque = 5.0f;

				my_jointR.AngularVelocity = MathLib.Max(my_jointR.AngularVelocity, 0.0f);
				my_jointR.AngularVelocity -= ifps * 50.0f;
				my_jointR.AngularTorque = 5.0f;
				
			//	my_jointR.AngularVelocity = -3.0f;
			//	my_jointR.AngularTorque = -3.0f;
			//	my_jointL.AngularVelocity = -3.0f;
			//	my_jointL.AngularTorque = -3.0f;

				//targetWheelL.ObjectBodyRigid.Frozen = true;
				//targetWheelR.ObjectBodyRigid.Frozen = true;

			//	my_jointL.WheelMass = 100f;

			}

			if (Input.IsKeyPressed(moveLeftKey))
			{
              //my_jointR.AngularDamping = 20000.0f;
			}
			if (Input.IsKeyPressed(moveRightKey))
			{
              //my_jointL.AngularDamping = 20000.0f;
			}

            if (Input.IsKeyPressed(jumpKey))
			{
				targetCar.ObjectBodyRigid.AddLinearImpulse(vec3.UP);
				//targetCar.ObjectBodyRigid.AddLinearImpulse(impulse * (acceleration * ifps * shape.Mass));
			}
		
		    if (Input.IsKeyPressed(stRightKey))
			{
				targetCar.ObjectBodyRigid.AddLinearImpulse(vec3.RIGHT);
				//targetCar.ObjectBodyRigid.AddLinearImpulse(impulse * (acceleration * ifps * shape.Mass));
			}

	}
}