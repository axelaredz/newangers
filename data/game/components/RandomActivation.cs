﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "9c48235f5e779932a56da6e9a042433456a71503")]
public class RandomActivation : Component
{
	[ShowInEditor][Parameter(Tooltip = "Collection of nodes, the elements of which are activated")]
	private Node[] nodes = null;

	[ShowInEditor][ParameterSlider(Min = 0, Tooltip = "Min delay time")]
	private float minTime = 0;

	[ShowInEditor][ParameterSlider(Min = 0, Tooltip = "Max delay time")]
	private float maxTime = 0;

	// timers for every object
	private float[] timers = null;

	void Init()
	{
		timers = new float[nodes.Length];

		// init objects and start timers
		for (int i = 0; i < nodes.Length; i++)
		{
			if (Game.GetRandomInt(0, 2) == 0)
				nodes[i].Enabled = false;
			else
				nodes[i].Enabled = true;

			timers[i] = Game.GetRandomFloat(minTime, maxTime);
		}
	}
	
	void Update()
	{
		// update timers and objects' states
		float ifps = Game.IFps;
		for (int i = 0; i < timers.Length; i++)
		{
			timers[i] -= ifps;
			if (timers[i] < 0)
			{
				nodes[i].Enabled = !nodes[i].Enabled;
				timers[i] = Game.GetRandomFloat(minTime, maxTime);
			}
		}
	}
}
