﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "0a8c66c3bf8cdd46b29613ed0c236468c2647ca7")]
public class Bullet : Component
{
	[ShowInEditor][ParameterSlider(Min = 0.0f, Tooltip = "Speed of bullet movement")]
	private float speed = 10f;

	[ShowInEditor][ParameterSlider(Min = 0, Tooltip = "Damage value that is applied to health")]
	private int damage = 1;

	[ShowInEditor][ParameterMask(Tooltip = "Bitmask for detecting intersection with objects")]
	private int intersectionMask = 0B00001111;

	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains hit effect")]
	private string bulletHitEffect = "";

	static private WorldIntersectionNormal intersection = null;

	void Init()
	{
		if (intersection == null)
			intersection = new WorldIntersectionNormal();

		// add offset to exclude self-damage
		vec3 dir = node.GetWorldDirection(MathLib.AXIS.Y);
		node.WorldTranslate(dir * speed * Game.IFps * 0.5f);
	}

	void Update()
	{
		vec3 oldPos = node.WorldPosition;
		vec3 dir = node.GetWorldDirection(MathLib.AXIS.Y);

		// set bullet movement
		node.WorldTranslate(dir * speed * Game.IFps);

		// check intersection with other objects
		Unigine.Object obj = World.GetIntersection(oldPos, node.WorldPosition, intersectionMask, intersection);
		if (obj != null)
		{
			// create a node that contains hit effect
			Node hitEffect = World.LoadNode(bulletHitEffect);
			hitEffect.WorldPosition = intersection.Point;
			hitEffect.SetWorldDirection(intersection.Normal, vec3.UP, MathLib.AXIS.Y);

			// add impulse to an object if it is a rigid body
			BodyRigid rb = obj.BodyRigid;
			if (rb != null)
			{
				rb.Frozen = false;
				rb.AddWorldImpulse(obj.WorldPosition, node.GetWorldDirection(MathLib.AXIS.Y) * speed);
			}

			// take damage
			HealthController healthController = ComponentSystem.GetComponent<HealthController>(obj);
			if (healthController != null)
				healthController.TakeDamage(damage);

			// remove a bullet
			node.DeleteLater();

			return;
		}
	}
}
