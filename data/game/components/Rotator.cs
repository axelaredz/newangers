﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "79f00e727c831b2972eaaa743797d3432edc5fc6")]
public class Rotator : Component
{
	[ShowInEditor][Parameter(Tooltip = "Angular rotation speed")]
	private vec3 angularSpeed = vec3.ZERO;

	void Init()
	{
		node.SetRotation(new quat(0, 0, 0));
	}

	void Update()
	{
		// apply rotation
		vec3 delta = angularSpeed * Game.IFps;
		node.SetWorldRotation(node.GetWorldRotation() * new quat(delta.x, delta.y, delta.z));
	}
}
