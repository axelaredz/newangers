﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "eae66d3ca484676b71cfd5ceb44ae2d5616a5630")]
public class Camera : Component
{
	[ShowInEditor][Parameter(Tooltip = "Point of interest")]
	private Node target = null;

	[ShowInEditor][ParameterSlider(Min = 0.1f, Max = 100.0f, Tooltip = "Initial distance to the target")]
	private float initDistance = 9.0f;

	[ShowInEditor][ParameterSlider(Min = 0.0f, Max = 360.0f, Tooltip = "Initial azimuth angle")]
	private float initPhi = 60.0f;

	[ShowInEditor][ParameterSlider(Min = -89.9f, Max = 89.9f, Tooltip = "Initial elevation angle")]
	private float initTheta = 70.0f;

	[ShowInEditor][ParameterSlider(Min = -89.9f, Max = 89.9f, Tooltip = "Minimum elevation angle")]
	private float minTheta = 0.0f;

	[ShowInEditor][ParameterSlider(Min = -89.9f, Max = 89.9f, Tooltip = "Maximum elevation angle")]
	private float maxTheta = 89.9f;

	[ShowInEditor][ParameterSlider(Min = 0.1f, Max = 100.0f, Tooltip = "Minimum distance to the target")]
	private float minDistance = 3.0f;

	[ShowInEditor][ParameterSlider(Min = 0.1f, Max = 100.0f, Tooltip = "Maximum distance to the target")]
	private float maxDistance = 15.0f;

	[ShowInEditor][ParameterSlider(Min = 0.0f, Tooltip = "Camera zoom speed")]
	private float zoomSpeed = 1.0f;

	[ShowInEditor][ParameterSlider(Min = 0.0f, Tooltip = "Speed of the angle shift")]
	private float turningSpeed = 1.0f;

	[ShowInEditor][Parameter(Tooltip = "Fixed camera angles")]
	private bool isFixedAngles = false;

	private PlayerDummy camera;

	// real distance from the camera to the target
	private float cameraDistance;

	// interpolated distance from the camera to the target
	private float cameraCurrentDistance;

	// the last valid position of the camera target
	private vec3 lastTargetPosition;

	// current camera angles
	private float theta = 0;
	private float phi = 0;

	private const float interpolationFactor = 5.0f;

	void Init()
	{
		// set the mouse behaviour
		Input.MouseHandle = Input.MOUSE_HANDLE.GRAB;
		App.MouseGrab = true;

		// get the camera from the current node
		camera = node as PlayerDummy;

		// check the target
		lastTargetPosition = vec3.ZERO;
		if (target != null && Node.IsNode(target) == 1)
			lastTargetPosition = target.WorldPosition;

		// calculate the camera's direction and position
		vec3 dir = camera.WorldPosition - lastTargetPosition;
		cameraDistance = MathLib.Clamp(initDistance, minDistance, maxDistance);
		cameraCurrentDistance = cameraDistance;

		phi = initPhi;
		theta = initTheta;

		dir = new quat(vec3.UP, phi) * vec3.FORWARD;
		dir = new quat(MathLib.Cross(vec3.UP, dir), -theta) * dir;

		dir.Normalize();

		// set the camera's position and direction
		camera.WorldPosition = lastTargetPosition + dir * cameraCurrentDistance;
		camera.ViewDirection = -dir;
	}

	void Update()
	{
		// update the mouse behaviour
		if (Input.MouseHandle == Input.MOUSE_HANDLE.GRAB && Input.IsKeyDown(Input.KEY.ESC))
			Input.MouseHandle = Input.MOUSE_HANDLE.USER;

		if (UiElement.AnyIsSelect == false && Input.IsMouseButtonDown(Input.MOUSE_BUTTON.LEFT) && Input.MouseHandle == Input.MOUSE_HANDLE.USER)
			Input.MouseHandle = Input.MOUSE_HANDLE.GRAB;

		// update a valid target position
		if (target != null && Node.IsNode(target) == 1)
			lastTargetPosition = target.WorldPosition;

		// update camera angles
		if (!isFixedAngles && Render.FirstFrame == false)
		{
			phi -= ControlsApp.MouseDX * turningSpeed;
			theta = MathLib.Clamp(theta + ControlsApp.MouseDY * turningSpeed, minTheta, maxTheta);
		}

		// update the real distance from the camera to the target
		int axis = Input.MouseWheel;
		if (axis != 0)
			cameraDistance = MathLib.Clamp(cameraDistance - axis * zoomSpeed, minDistance, maxDistance);

		// update the camera direction
		vec3 dir = new quat(vec3.UP, phi) * vec3.FORWARD;
		dir = new quat(MathLib.Cross(vec3.UP, dir), -theta) * dir;

		// interpolate the distance from the camera to the target and set the camera position and direction
		cameraCurrentDistance = MathLib.Lerp(cameraCurrentDistance, cameraDistance, interpolationFactor * Game.IFps);
		camera.WorldPosition = lastTargetPosition + dir.Normalize() * cameraCurrentDistance;
		camera.ViewDirection = -dir;
	}
}
