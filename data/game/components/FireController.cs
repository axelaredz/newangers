﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "8c8c44a5f7052dc04facc61f782c5cf2175a17a9")]
public class FireController : Component
{
	[ShowInEditor][ParameterSlider(Min = 0.0f, Tooltip = "Time interval between shots, in seconds")]
	private float timeInterval = 0.1f;

	[ShowInEditor][Parameter(Tooltip = "Use only the horizontal plane for the bullet movement")]
	private bool useOnlyHorizontal = true;

	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains the bullet")]
	private string bulletReference = "";

	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains the spawn effect")]
	private string spawnEffectReference = "";

	[ShowInEditor][Parameter(Tooltip = "Object representing the right gun attached to the bone")]
	private NodeDummy rightGunBone = null;

	[ShowInEditor][Parameter(Tooltip = "Object representing the left gun attached to the bone")]
	private NodeDummy leftGunBone = null;

	[ShowInEditor][Parameter(Tooltip = "Object for animation")]
	private ObjectMeshSkinned mesh = null;

	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains the idle animation")]
	private string idleAnim = "";

	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains the left shoot animation")]
	private string leftShootAnim = "";

	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains the right shoot animation")]
	private string rightShootAnim = "";

	[ShowInEditor][ParameterSlider(Min = 0.0f, Tooltip = "Animation duration, in seconds")]
	private float shootAniamtionTime = 0.1f;

	private enum Side { Left, Right }
	private enum Layer { Idle, Left, Right, LeftReferenceFrame, RightReferenceFrame, LayersCount }

	private float lastFireTime = 0.0f;
	private float shootTime = 0.0f;
	private float idleAnimationSpeed = 30.0f;

	private Side currentSide = Side.Left;

	void Init()
	{
		lastFireTime = Game.Time;

		// set animation on different layers
		mesh.NumLayers = (int)Layer.LayersCount;
		mesh.SetAnimation((int)Layer.Idle, idleAnim);
		mesh.SetAnimation((int)Layer.Left, leftShootAnim);
		mesh.SetAnimation((int)Layer.Right, rightShootAnim);

		// set inverse transform for blend calculation on auxiliary layers
		mesh.SetFrame((int)Layer.Idle, 0);
		mesh.InverseLayer((int)Layer.LeftReferenceFrame, (int)Layer.Idle);
		mesh.InverseLayer((int)Layer.RightReferenceFrame, (int)Layer.Idle);

		shootAniamtionTime = MathLib.Min(shootAniamtionTime, timeInterval);
		idleAnimationSpeed = mesh.Speed;
	}

	void Update()
	{
		// set the idle animation in the required frame
		mesh.SetFrame((int)Layer.Idle, Game.Time * idleAnimationSpeed);

		if (shootTime > shootAniamtionTime)
			return;

		shootTime += Game.IFps;

		if (currentSide == Side.Left)
		{
			// set the layer to the corresponding frame of animation
			mesh.SetFrame((int)Layer.Right, (mesh.GetNumFrames((int)Layer.Right) - 1) * MathLib.Saturate(shootTime / shootAniamtionTime));

			// find local transformations of all bones relative to the frame 0 of the Right shoot animation
			mesh.MulLayer((int)Layer.Right, (int)Layer.RightReferenceFrame, (int)Layer.Right);

			// add local transform﻿ations of Right shoot animation bones to the idle animation
			mesh.MulLayer((int)Layer.Idle, (int)Layer.Idle, (int)Layer.Right);
		}
		else
		{
			// set the layer to the corresponding frame of animation
			mesh.SetFrame((int)Layer.Left, (mesh.GetNumFrames((int)Layer.Left) - 1) * MathLib.Saturate(shootTime / shootAniamtionTime));

			// find local transformations of all bones relative to the frame 0 of the Left shoot animation
			mesh.MulLayer((int)Layer.Left, (int)Layer.LeftReferenceFrame, (int)Layer.Left);

			// add local transform﻿ations of Right shoot animation bones to the idle animation
			mesh.MulLayer((int)Layer.Idle, (int)Layer.Idle, (int)Layer.Left);
		}
	}

	public void Fire()
	{
		if (Game.Time - lastFireTime > timeInterval)
		{
			// create an instance of the bullet
			Node bullet = World.LoadNode(bulletReference);
			Node spawnEffect = World.LoadNode(spawnEffectReference);

			Node parent = node;
			spawnEffect.SetWorldParent(parent);

			// set the corresponding transformation and the parent
			switch (currentSide)
			{
				case Side.Left:
					bullet.WorldTransform = leftGunBone.WorldTransform;
					spawnEffect.WorldTransform = leftGunBone.WorldTransform;
					currentSide = Side.Right;
					break;

				case Side.Right:
					bullet.WorldTransform = rightGunBone.WorldTransform;
					spawnEffect.WorldTransform = rightGunBone.WorldTransform;
					currentSide = Side.Left;
					break;
			}

			// set the forward direction for the bullet movement in the horizontal plane
			vec3 dirY = bullet.GetWorldDirection(MathLib.AXIS.Y);

			if (useOnlyHorizontal)
				dirY.z = 0;

			dirY.Normalize();

			bullet.SetWorldDirection(dirY, vec3.UP, MathLib.AXIS.Y);
			spawnEffect.SetWorldDirection(dirY, vec3.UP, MathLib.AXIS.Y);

			lastFireTime = Game.Time;
			shootTime = 0.0f;
		}
	}
}
