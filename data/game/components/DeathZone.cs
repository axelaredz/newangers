﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "89f652096581b3d4a15f8d2348f010262ac4e94d")]
public class DeathZone : Component
{
	// the area into which an object should fall
	private WorldTrigger trigger = null;

	void Init()
	{
		trigger = node as WorldTrigger;

		// set the handler to be executed when an object enters the area
		trigger.AddEnterCallback(Enter);
	}

	void Enter(Node target)
	{
		// try to get the node's health controller and apply damage
		HealthController healthController = ComponentSystem.GetComponent<HealthController>(target);
		healthController?.TakeDamage(healthController.HP);
	}
}
