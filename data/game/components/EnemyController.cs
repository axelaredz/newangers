﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "c1b5d7138737715642cdf8bdfc74e2f8cb0549f7")]
public class EnemyController : Component
{
	[ShowInEditor][Parameter(Tooltip = "Navigation mesh object")]
	private NavigationMesh navMesh = null;

	[ShowInEditor][Parameter(Tooltip = "Enemy movement speed")]
	private float speed = 3.0f;

	[ShowInEditor][Parameter(Tooltip = "Player detection distance")]
	private float detectionDistance = 10.0f;

	[ShowInEditor][Parameter(Tooltip = "Minimum distance to the player")]
	private float minDistance = 5.0f;

	[ShowInEditor][Parameter(Tooltip = "Distance starting from which the enemy fires")]
	private float fireDistance = 7.0f;

	[ShowInEditor][Parameter(Tooltip = "Target for enemy")]
	private Node target = null;

	[ShowInEditor][Parameter(Tooltip = "Enemy body, which can rotate")]
	private Node body = null;

	[ShowInEditor][Parameter(Tooltip = "Enemy fire controller")]
	private FireController fireController = null;

	[ShowInEditor][ParameterMask(Tooltip = "Bitmask to detect the player, which contains both environment objects and the player")]
	private int visibilityOfPlayer = 0B00000011;

	[ShowInEditor][ParameterMask(Tooltip = "Bitmask for detecting intersection with the player")]
	private int playerMask = 0B00000010;

	[ShowInEditor][Parameter(Tooltip = "Enemy health controller")]
	private HealthController healthController = null;

	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains explosion effect")]
	private string explosionEffectReference = null;

	private BodyRigid rb;
	private WorldIntersection intersection;

	// for calculation of the path in the navigation mesh
	private PathRoute route;
	private bool needCreate;

	// for saving the last correct path
	private Stack<vec3> points;

	// the min distance to check if a point was visited
	private float pointVisitDelta = 1.0f;

	// timers for update
	private float updateInterval = 3.0f;
	private float lastUpdate = 0.0f;

	// enemy radius in the navigation mesh
	float radius = 0.3f;

	// enemy movement parameters
	private vec3 lastVector;
	private vec3 newVelocity;

	// the node and target positions that should be contained in the navigation mesh
	private vec3 lastNodeValidPos;
	private vec3 lastTargetValidPos;

	void Init()
	{
		// create the navigation route and set parameters
		route = new PathRoute(radius);
		route.Height = 1;
		route.MaxAngle = 0.5f;

		points = new Stack<vec3>();

		rb = node.ObjectBodyRigid;

		// start calculating the route
		route.Create2D(node.WorldPosition, target.WorldPosition, 1);
		lastVector = vec3.ZERO;
		needCreate = true;
		lastUpdate = 0;

		intersection = new WorldIntersection();

		// set the dead handler
		healthController.deadEvent += DeadEventHandler;
	}

	void Update()
	{
		// check the target
		if (target == null || Node.IsNode(target) == 0)
			return;

		// check and update positions of the node and the target in the navigation mesh
		if (navMesh.Inside2D(node.WorldPosition, radius) == 1)
			lastNodeValidPos = node.WorldPosition;

		if (navMesh.Inside2D(target.WorldPosition, radius) == 1)
			lastTargetValidPos = target.WorldPosition;

		// check the distance to the target
		float distanceToTarget = (node.WorldPosition - target.WorldPosition).Length;
		if (detectionDistance > distanceToTarget && distanceToTarget > minDistance)
		{
			// set the next target point
			if (points.Count > 0)
			{
				// get the distance between the enemy position and the point along the route
				vec3 distance = node.WorldPosition - points.Peek();
				distance.z = 0;

				// if the enemy is near the point, remove the point from the route
				if (distance.Length < pointVisitDelta)
					points.Pop();
			}

			// set a new direction to the next point along the route
			if (points.Count > 0)
			{
				lastVector = points.Peek() - node.WorldPosition;
				lastVector.z = 0;
				lastVector.Normalize();
			}
			else
				lastVector = vec3.ZERO;

			// update the route
			if (Game.Time - lastUpdate > updateInterval)
			{
				// calculate a new route or update points of the route
				if (needCreate)
				{
					route.Create2D(lastNodeValidPos, lastTargetValidPos, 1);
					needCreate = false;
				}
				else if (route.IsReady)
				{
					// if the enemy can't reach the final point, then try to calculate again
					if (!route.IsReached)
						route.Create2D(lastNodeValidPos, lastTargetValidPos, 1);
					else
					{
						// if calculated successfully, then update points of the current path
						points.Clear();
						for (int i = route.NumPoints - 1; i >= 0; i--)
							points.Push(route.GetPoint(i));

						needCreate = true;
						lastUpdate = Game.Time;
					}
				}
			}
		}
		else
		{
			lastVector = vec3.ZERO;
			points.Clear();
		}

		// update the velocity
		newVelocity.x = MathLib.Lerp(newVelocity.x, lastVector.x * speed, 0.3f);
		newVelocity.y = MathLib.Lerp(newVelocity.y, lastVector.y * speed, 0.3f);

		// update the body's direction
		vec3 currentDirection = body.GetWorldDirection(MathLib.AXIS.Y);
		vec3 targetDirection = vec3.ZERO;

		if (lastVector.Length2 > MathLib.EPSILON)
			targetDirection = lastVector;
		else
		{
			targetDirection = (target.WorldPosition - node.WorldPosition);
			targetDirection.z = 0;
			targetDirection.Normalize();
		}

		quat currentRot = new quat(MathLib.LookAt(vec3.ZERO, currentDirection, vec3.UP, MathLib.AXIS.Y));
		quat targetRot = new quat(MathLib.LookAt(vec3.ZERO, targetDirection, vec3.UP, MathLib.AXIS.Y));

		quat rot = MathLib.Slerp(currentRot, targetRot, 7.5f * Game.IFps);
		quat delta = rot * MathLib.Inverse(currentRot);

		currentDirection = currentDirection * delta;
		currentDirection.z = 0;
		currentDirection.Normalize();

		body.SetWorldDirection(currentDirection, vec3.UP, MathLib.AXIS.Y);

		// check the distance to the target and its visibility for fire
		if ((node.WorldPosition - target.WorldPosition).Length < fireDistance)
		{
			Unigine.Object obj = World.GetIntersection(node.WorldPosition, target.WorldPosition, visibilityOfPlayer, intersection);

			if (obj != null && obj.GetIntersectionMask(0) == playerMask)
				fireController.Fire();
		}
	}

	void UpdatePhysics()
	{
		// update the enemy speed
		vec3 velocity = rb.LinearVelocity;
		velocity.x = newVelocity.x;
		velocity.y = newVelocity.y;

		rb.LinearVelocity = velocity;
	}

	void Shutdown()
	{
		// remove the dead handler
		healthController.deadEvent -= DeadEventHandler;
	}

	private void DeadEventHandler()
	{
		// play the explosion effect and delete the enemy
		Node effect = World.LoadNode(explosionEffectReference);
		effect.WorldPosition = node.WorldPosition;

		node.DeleteLater();
	}
}
