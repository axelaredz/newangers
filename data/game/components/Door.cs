﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "2252e22e1740c3b555600190f9fe67af6bc0f142")]
public class Door : Component
{
	[ShowInEditor][Parameter(Tooltip = "Door object for adding animations")]
	private ObjectMeshSkinned mesh = null;

	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains animation")]
	private string animation = "";

	[ShowInEditor][Parameter(Tooltip = "Animation duration")]
	private float animationTime = 1.0f;

	[ShowInEditor][Parameter(Tooltip = "Area into which a visitor (player, enemy, or any other object) should enter")]
	private WorldTrigger trigger = null;

	// current animation time
	private float currentTime = 0;

	// visitors (player, enemy, or any other object) count in the area
	private int visitorsCount = 0;

	// current state of the door
	private bool isOpen = false;

	private int frameCount = 0;

	void Init()
	{
		visitorsCount = 0;

		// set enter/leave handlers for the area
		trigger.AddEnterCallback(Enter);
		trigger.AddLeaveCallback(Leave);

		// set animation
		mesh.NumLayers = 1;
		mesh.SetAnimation(0, animation);
		mesh.SetLayerEnabled(0, true);

		frameCount = mesh.GetNumFrames(0) - 1;
		currentTime = animationTime;
		isOpen = false;
	}

	void Update()
	{
		// if the door needs to be animated
		if (currentTime > 0)
		{
			// check the current state and play open/close animation
			if (isOpen)
				mesh.SetFrame(0, frameCount * MathLib.Saturate(1.0f - currentTime / animationTime));
			else
				mesh.SetFrame(0, frameCount * MathLib.Saturate(currentTime / animationTime));

			currentTime -= Game.IFps;

			// when the animation time ends, set the final frame
			if (currentTime <= 0)
			{
				currentTime = 0;
				if (isOpen)
					mesh.SetFrame(0, frameCount * MathLib.Saturate(1.0f - currentTime / animationTime));
				else
					mesh.SetFrame(0, frameCount * MathLib.Saturate(currentTime / animationTime));
			}
		}
	}

	void Enter(Node target)
	{
		// try to get health controller of a visitor
		HealthController healthController = ComponentSystem.GetComponent<HealthController>(target);
		if (healthController != null)
		{
			// set the dead handler and remember a new visitor
			healthController.deadEvent += DeadTargetHandler;
			visitorsCount++;

			// if the area has been empty, start the door opening animation
			if (visitorsCount == 1)
			{
				isOpen = true;
				currentTime = animationTime - currentTime;
			}
		}
	}

	void Leave(Node target)
	{
		// try to get health controller of the player or an enemy
		HealthController healthController = ComponentSystem.GetComponent<HealthController>(target);
		if (healthController != null)
		{
			// remove the dead handler and forget a visitor
			healthController.deadEvent -= DeadTargetHandler;
			visitorsCount--;

			// if the visitor was the last to leave the area, start the door closing animation
			if (visitorsCount == 0)
			{
				isOpen = false;
				currentTime = animationTime - currentTime;
			}
		}
	}

	void DeadTargetHandler()
	{
		// if a visitor is dead and has not left the area, forget about that visitor
		visitorsCount--;
		if (visitorsCount < 0)
			visitorsCount = 0;

		// if this visitor was the last visitor in the area, start the door closing animation
		if (visitorsCount == 0)
		{
			isOpen = false;
			currentTime = animationTime - currentTime;
		}
	}
}
