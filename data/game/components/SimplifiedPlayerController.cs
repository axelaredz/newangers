﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "23bb1d046fe30493d123c0d6eefb023d9a075e78")]
public class SimplifiedPlayerController : Component
{
	// controls
	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Forward axis")]
	private Input.KEY forwardKey = Input.KEY.W;

	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Backward axis")]
	private Input.KEY backwardKey = Input.KEY.S;

	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Left axis")]
	private Input.KEY moveLeftKey = Input.KEY.A;

	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Right axis")]
	private Input.KEY moveRightKey = Input.KEY.D;

	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Jump button")]
	private Input.KEY jumpKey = Input.KEY.SPACE;

	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Run button")]
	private Input.KEY runKey = Input.KEY.SHIFT;

	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Fire button")]
	private Input.KEY fireKey = Input.KEY.F;

	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Mouse fire button")]
	private Input.MOUSE_BUTTON mouseFireKey = Input.MOUSE_BUTTON.LEFT;

	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Player fire controller")]
	private FireController fireController = null;

	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Player health controller")]
	private HealthController healthController = null;

	[ShowInEditor][Parameter(Group = "Control", Tooltip = "Look in direction of motion")]
	bool lookAtDirection = true;

	[ShowInEditor][ParameterFile(Group = "Effects", Tooltip = "Asset file that contains explosion effect")]
	private string explosionEffectReference = null;

	[ShowInEditor][Parameter(Group = "Active camera", Tooltip = "Current active camera")]
	private PlayerDummy playerDummy = null;

	[ShowInEditor][ParameterMask(Group = "Physical parameters")]
	private int physicalMask = 1;

	[ShowInEditor][ParameterMask(Group = "Physical parameters")]
	private int intersectionMask = 1;

	[ShowInEditor][ParameterMask(Group = "Physical parameters")]
	private int collisionMask = 1;

	[ShowInEditor][Parameter(Group = "Physical parameters")]
	private float physicalMass = 30.0f;

	[ShowInEditor][Parameter(Group = "Physical parameters")]
	private float radius = 0.5f;

	[ShowInEditor][Parameter(Group = "Physical parameters")]
	private float height = 1.2f;

	[ShowInEditor][Parameter(Group = "Physical parameters")]
	private float minFriction = 0.1f;

	[ShowInEditor][Parameter(Group = "Physical parameters")]
	private float maxFriction = 1.0f;

	[ShowInEditor][Parameter(Group = "Physical parameters")]
	private float minVelocity = 3.0f;

	[ShowInEditor][Parameter(Group = "Physical parameters")]
	private float maxVelocity = 6.0f;

	[ShowInEditor][Parameter(Group = "Physical parameters")]
	private float acceleration = 8.0f;

	[ShowInEditor][Parameter(Group = "Physical parameters")]
	private float damping = 8.0f;

	[ShowInEditor][Parameter(Group = "Physical parameters")]
	private float jumping = 0.5f;

	public const float PLAYER_ACTOR_IFPS = 1.0f / 60.0f;
	public const float PLAYER_ACTOR_CLAMP = 89.9f;
	public const int PLAYER_ACTOR_COLLISIONS = 4;

	public float PhysicalMass
	{
		get { return shape.Mass; }
		set { shape.Mass = value; }
	}

	public int PhysicalMask
	{
		get { return rigid.PhysicalMask; }
		set { rigid.PhysicalMask = value; }
	}

	public int PhysicsIntersectionMask
	{
		get { return shape.PhysicsIntersectionMask; }
		set { shape.PhysicsIntersectionMask = value; }
	}

	public int CollisionMask
	{
		get { return shape.CollisionMask; }
		set { shape.CollisionMask = value; }
	}

	public float MinFriction
	{
		get { return minFriction; }
		set { minFriction = MathLib.Max(value, 0.0f); }
	}

	public float MaxFriction
	{
		get { return maxFriction; }
		set { maxFriction = MathLib.Max(value, 0.0f); }
	}

	public float MinVelocity
	{
		get { return minVelocity; }
		set { minVelocity = MathLib.Max(value, 0.0f); }
	}

	public float MaxVelocity
	{
		get { return maxVelocity; }
		set { maxVelocity = MathLib.Max(value, 0.0f); }
	}

	public float Acceleration
	{
		get { return acceleration; }
		set { acceleration = MathLib.Max(value, 0.0f); }
	}

	public float Damping
	{
		get { return damping; }
		set { damping = MathLib.Max(value, 0.0f); }
	}

	public float Jumping
	{
		get { return jumping; }
		set { jumping = MathLib.Max(value, 0.0f); }
	}

	public float MaxStepHeight { get; set; }

	private Unigine.Camera camera;
	private ObjectDummy objectDummy;
	private BodyRigid rigid;
	private ShapeCapsule shape;

	private vec3 velocity;
	private vec3 position;
	private vec3 direction;
	private float phiAngle;

	private enum Side { Left, Right }
	private const float bulletOffset = 0.6f;

	enum State
	{
		Forward = 0,
		Backward,
		MoveLeft,
		MoveRight,
		Jump,
		Run,
		Fire,
		NumStates
	};

	enum StateStatus
	{
		Diasbled = 0,
		Enabled,
		Begin,
		End
	};

	private int[] states = new int[(int)State.NumStates];
	private float[] times = new float[(int)State.NumStates];

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// MAIN METHODS
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Init()
	{
		camera = playerDummy.Camera;

		objectDummy = new ObjectDummy();

		rigid = new BodyRigid();
		rigid.MaxAngularVelocity = 0.0f;
		rigid.Freezable = true;

		shape = new ShapeCapsule(radius, height);
		shape.Restitution = 0.0f;
		shape.Continuous = false;

		rigid.Enabled = true;
		objectDummy.Body = rigid;
		shape.Body = rigid;

		position = vec3.ZERO;
		direction = new vec3(1.0f, 0.0f, 0.0f);
		phiAngle = 90.0f;

		for (int i = 0; i < (int)State.NumStates; i++)
		{
			states[i] = 0;
			times[i] = 0.0f;
		}

		UpdateTransform();

		PhysicalMask = physicalMask;
		PhysicsIntersectionMask = intersectionMask;
		CollisionMask = collisionMask;
		PhysicalMass = physicalMass;
		SetCollisionRadius(radius);
		SetCollisionHeight(height);
		MinFriction = minFriction;
		MaxFriction = maxFriction;
		MinVelocity = minVelocity;
		MaxVelocity = maxVelocity;
		Acceleration = acceleration;
		Damping = damping;
		Jumping = jumping;

		Ground = 0;
		Ceiling = 0;

		healthController.deadEvent += DeadEventHandler;
	}

	public void Update()
	{
		float ifps = Game.IFps;

		UpdateRigid(ifps);
	}

	void Shutdown()
	{
		healthController.deadEvent -= DeadEventHandler;
	}

	private void DeadEventHandler()
	{
		Node effect = World.LoadNode(explosionEffectReference);
		effect.WorldPosition = node.WorldPosition;

		node.DeleteLater();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// PUBLIC METHODS
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// transform
	// (note: use these methods if you want to change position of the player.
	// node.WorldTransform doesn't work)
	public void SetTransform(mat4 transform)
	{
		node.Transform = transform;
		UpdateTransform();
	}

	public void SetWorldTransform(mat4 transform)
	{
		node.WorldTransform = transform;
		UpdateTransform();
	}

	public void SetCollisionRadius(float radius)
	{
		if (MathLib.Compare(shape.Radius, radius) < 1)
		{
			vec3 up = vec3.UP;

			rigid.SetPreserveTransform(new mat4(MathLib.Translate(up * (radius - shape.Radius))) * rigid.Transform);
			shape.Radius = radius;
		}
	}

	public float GetCollisionRadius()
	{
		return shape.Radius;
	}

	public void SetCollisionHeight(float height)
	{
		if (MathLib.Compare(shape.Height, height) < 1)
		{
			vec3 up = vec3.UP;

			rigid.SetPreserveTransform(new mat4(MathLib.Translate(up * (height - shape.Height) * 0.5f)) * rigid.Transform);
			shape.Height = height;
		}
	}

	public float GetCollisionHeight()
	{
		return shape.Height;
	}

	// phi angle
	public void SetPhiAngle(float angle)
	{
		angle = angle - phiAngle;
		direction = new quat(vec3.UP, angle) * direction;
		phiAngle += angle;

		FlushTransform();
	}

	public float GetPhiAngle()
	{
		return phiAngle;
	}

	// view direction
	public void SetViewDirection(vec3 view)
	{
		direction = MathLib.Normalize(view);

		// ortho basis
		vec3 tangent, binormal;
		Geometry.OrthoBasis(vec3.UP, out tangent, out binormal);

		// decompose direction
		phiAngle = MathLib.Atan2(MathLib.Dot(direction, tangent), MathLib.Dot(direction, binormal)) * MathLib.RAD2DEG;

		FlushTransform();
	}

	// ground flag
	public int Ground { get; set; }

	// ceiling flag
	public int Ceiling { get; set; }

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// PRIVATE METHODS
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// the player's transformation
	private mat4 GetBodyTransform()
	{
		vec3 up = vec3.UP;
		vec3 center = position;
		return MathLib.SetTo(center, center + new vec3(direction - up * MathLib.Dot(direction, up)), up) * new mat4(MathLib.RotateX(-90.0f) * MathLib.RotateZ(90.0f));
	}

	private mat4 GetModelview()
	{
		vec3 up = vec3.UP;
		vec3 eye = position + new vec3(up * (shape.Height + shape.Radius));
		return MathLib.LookAt(eye, eye + new vec3(direction), up);
	}

	// update the status of a state
	private int UpdateState(bool condition, int state, int begin, int end, float ifps)
	{
		// switching the status from Disabled to Begin
		if (condition && states[state] == (int)StateStatus.Diasbled && begin == 1)
		{
			states[state] = (int)StateStatus.Begin;
			times[state] = 0.0f;
			return (int)StateStatus.Begin;
		}

		// switching the status from Enabled or Begin to End
		if (!condition && (states[state] == (int)StateStatus.Enabled || states[state] == (int)StateStatus.Begin) && end == 1)
		{
			states[state] = (int)StateStatus.End;
			return (int)StateStatus.End;
		}

		// switching the status from Begin to Enabled
		if ((condition && states[state] == (int)StateStatus.Begin) || states[state] == (int)StateStatus.Enabled)
		{
			states[state] = (int)StateStatus.Enabled;
			times[state] += ifps;
			return (int)StateStatus.Enabled;
		}

		// switching the status from End to Disabled
		if (states[state] == (int)StateStatus.End)
		{
			states[state] = (int)StateStatus.Diasbled;
			return (int)StateStatus.Diasbled;
		}

		return (int)StateStatus.Diasbled;
	}

	private void UpdateStates(int enabled, float ifps)
	{
		// handle input events
		if (enabled == 1 && !Unigine.Console.Activity)
		{
			if (Input.IsKeyPressed(forwardKey) && Input.IsKeyPressed(backwardKey))
			{
				UpdateState(false, (int)State.Forward, 1, 1, ifps);
				UpdateState(false, (int)State.Backward, 1, 1, ifps);
			}
			else
			{
				UpdateState(Input.IsKeyPressed(forwardKey), (int)State.Forward, 1, 1, ifps);
				UpdateState(Input.IsKeyPressed(backwardKey), (int)State.Backward, 1, 1, ifps);
			}

			if (Input.IsKeyPressed(moveLeftKey) && Input.IsKeyPressed(moveRightKey))
			{
				UpdateState(false, (int)State.MoveLeft, 1, 1, ifps);
				UpdateState(false, (int)State.MoveRight, 1, 1, ifps);
			}
			else
			{
				UpdateState(Input.IsKeyPressed(moveLeftKey), (int)State.MoveLeft, 1, 1, ifps);
				UpdateState(Input.IsKeyPressed(moveRightKey), (int)State.MoveRight, 1, 1, ifps);
			}

			UpdateState(Input.IsKeyPressed(jumpKey), (int)State.Jump, Ground, 1, ifps);
			UpdateState(Input.IsKeyPressed(runKey), (int)State.Run, 1, 1, ifps);
			UpdateState(Input.MouseHandle == Input.MOUSE_HANDLE.GRAB &&
						(Input.IsKeyPressed(fireKey) || Input.IsMouseButtonPressed(mouseFireKey)), (int)State.Fire, 1, 1, ifps);
		}

		// disable states
		else
		{
			UpdateState(false, (int)State.Forward, 1, 1, ifps);
			UpdateState(false, (int)State.Backward, 1, 1, ifps);
			UpdateState(false, (int)State.MoveLeft, 1, 1, ifps);
			UpdateState(false, (int)State.MoveRight, 1, 1, ifps);
			UpdateState(false, (int)State.Jump, Ground, Ground, ifps);
			UpdateState(false, (int)State.Run, 1, 1, ifps);
			UpdateState(false, (int)State.Fire, 1, 1, ifps);
		}
	}

	// update controls
	private void UpdateRigid(float ifps)
	{
		vec3 up = vec3.UP;

		// impulse
		vec3 impulse = vec3.ZERO;

		// ortho basis
		vec3 tangent, binormal;
		Geometry.OrthoBasis(up, out tangent, out binormal);

		// current basis
		vec3 x = new quat(up, -phiAngle) * binormal;
		vec3 y = MathLib.Normalize(MathLib.Cross(up, x));
		vec3 z = MathLib.Normalize(MathLib.Cross(x, y));

		// controls

		// handle states
		UpdateStates(1, ifps);

		// old velocity
		float xVelocity = MathLib.Dot(x, rigid.LinearVelocity);
		float yVelocity = MathLib.Dot(y, rigid.LinearVelocity);
		float zVelocity = MathLib.Dot(z, rigid.LinearVelocity);

		// direction
		phiAngle = -playerDummy.GetWorldRotation().GetAngle(up) - 90;

		// new basis
		x = new quat(up, -phiAngle) * binormal;
		y = MathLib.Normalize(MathLib.Cross(up, x));
		z = MathLib.Normalize(MathLib.Cross(x, y));

		// movement
		if (states[(int)State.Forward] > 0)
			impulse += x;
		if (states[(int)State.Backward] > 0)
			impulse -= x;
		if (states[(int)State.MoveLeft] > 0)
			impulse += y;
		if (states[(int)State.MoveRight] > 0)
			impulse -= y;
		impulse.Normalize();

		// unfreeze body
		if (impulse.Length2 > MathLib.EPSILON)
			rigid.Frozen = false;

		// fire
		if (states[(int)State.Fire] > 0)
			fireController.Fire();

		// look at direction
		if (impulse.Length2 > 0 && lookAtDirection)
		{
			quat currentRot = new quat(MathLib.LookAt(vec3.ZERO, direction, vec3.UP, MathLib.AXIS.Y));
			quat targetRot = new quat(MathLib.LookAt(vec3.ZERO, impulse, vec3.UP, MathLib.AXIS.Y));

			quat rot = MathLib.Slerp(currentRot, targetRot, 7.5f * ifps);
			quat delta = rot * MathLib.Inverse(currentRot);

			direction = direction * delta;
			direction.Normalize();
		}

		// velocity
		if (states[(int)State.Run] > 0)
			impulse *= maxVelocity;
		else
			impulse *= minVelocity;

		// jump
		if (states[(int)State.Jump] == (int)StateStatus.Begin)
		{
			rigid.Frozen = false;
			impulse += z * MathLib.Fsqrt(2.0f * 9.8f * jumping) / (acceleration * ifps);
		}

		// rotate velocity
		if (Ground > 0)
			rigid.LinearVelocity = x * xVelocity + y * yVelocity + z * zVelocity;

		// target velocity
		float targetVelocity = MathLib.Length(new vec2(MathLib.Dot(x, impulse), MathLib.Dot(y, impulse)));

		// save old velocity
		velocity = rigid.LinearVelocity;
		float oldVelocity = MathLib.Length(new vec2(MathLib.Dot(x, velocity), MathLib.Dot(y, velocity)));

		// integrate velocity
		rigid.AddLinearImpulse(impulse * (acceleration * ifps * shape.Mass));

		// damping
		float currentVelocity = MathLib.Length(new vec2(MathLib.Dot(x, rigid.LinearVelocity), MathLib.Dot(y, rigid.LinearVelocity)));
		if (targetVelocity < MathLib.EPSILON || currentVelocity > targetVelocity)
		{
			vec3 linearVelocity = z * MathLib.Dot(z, rigid.LinearVelocity);
			linearVelocity += (x * MathLib.Dot(x, rigid.LinearVelocity) + y * MathLib.Dot(y, rigid.LinearVelocity)) * MathLib.Exp(-damping * ifps);
			rigid.LinearVelocity = linearVelocity;
		}

		// clamp the maximum velocity
		currentVelocity = MathLib.Length(new vec2(MathLib.Dot(x, rigid.LinearVelocity), MathLib.Dot(y, rigid.LinearVelocity)));
		if (currentVelocity > oldVelocity)
		{
			if (currentVelocity > targetVelocity)
			{
				vec3 linearVelocity = z * MathLib.Dot(z, rigid.LinearVelocity);
				linearVelocity += (x * MathLib.Dot(x, rigid.LinearVelocity) + y * MathLib.Dot(y, rigid.LinearVelocity)) * targetVelocity / currentVelocity;
				rigid.LinearVelocity = linearVelocity;
			}
		}

		// frozen velocity
		if (currentVelocity < MathLib.EPSILON)
			rigid.LinearVelocity = z * MathLib.Dot(z, rigid.LinearVelocity);

		// friction
		if (targetVelocity < MathLib.EPSILON)
			shape.Friction = maxFriction;
		else
			shape.Friction = minFriction;

		// clear collision flags
		Ground = 0;
		Ceiling = 0;

		// get collision
		vec3 cap0 = shape.BottomCap;
		vec3 cap1 = shape.TopCap;
		for (int i = 0; i < rigid.NumContacts; i++)
		{
			vec3 point = rigid.GetContactPoint(i);
			vec3 normal = rigid.GetContactNormal(i);
			if (MathLib.Dot(normal, up) > 0.5f && MathLib.Dot(new vec3(point - cap0), up) < 0.0f)
				Ground = 1;
			if (MathLib.Dot(normal, up) < -0.5f && MathLib.Dot(new vec3(point - cap1), up) > 0.0f)
				Ceiling = 1;
		}

		// current parameters
		position = objectDummy.WorldTransform.GetColumn3(3);

		FlushTransform();
	}

	// update transformation
	private void UpdateTransform()
	{
		// update transformation
		vec3 up = vec3.UP;

		// ortho basis
		vec3 tangent, binormal;
		Geometry.OrthoBasis(up, out tangent, out binormal);

		// decompose transformation
		position = node.WorldTransform.GetColumn3(3);
		direction = MathLib.Normalize(new vec3(node.WorldTransform.GetColumn3(1)));

		// decompose direction
		phiAngle = MathLib.Atan2(MathLib.Dot(direction, tangent), MathLib.Dot(direction, binormal)) * MathLib.RAD2DEG;

		// object transformation
		objectDummy.WorldTransform = GetBodyTransform();
	}

	// flush
	private void FlushTransform()
	{
		vec3 up = vec3.UP;
		node.WorldTransform = MathLib.SetTo(position, position + new vec3(direction - up * MathLib.Dot(direction, up)), up) * new mat4(MathLib.RotateX(-90.0f));
	}
}
