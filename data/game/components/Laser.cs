﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "1d1b36ade57ea828c6c56d67ad1b8b9aa27e6631")]
public class Laser : Component
{
	[ShowInEditor][ParameterSlider(Min = 0, Tooltip = "Max possible laser length")]
	private float maxLength = 1;

	[ShowInEditor][ParameterFile(Tooltip = "File that contains the asset to be used as visualization of the hit point (place where the laser intersects anything)")]
	private string hitPointReference = "";

	[ShowInEditor][Parameter(Tooltip = "Damage value that is applied to health")]
	private int damage = 1;

	[ShowInEditor][Parameter(Tooltip = "Interval between applying damage, in seconds")]
	private float damageTimeInterval = 0.1f;

	private const int generalBitMask = 0B11111111; 
	private float currentTime = 0;

	private Node hitPoint = null;

	static private WorldIntersectionNormal intersection = null;

	void Init()
	{
		// create a hit point
		hitPoint = World.LoadNode(hitPointReference);

		if (intersection == null)
			intersection = new WorldIntersectionNormal();
	}

	[MethodUpdate(Order = 1)]
	void Update()
	{
		// get the laser's position and direction
		vec3 pos = node.WorldPosition;
		vec3 dir = node.GetWorldDirection(MathLib.AXIS.Y);

		// find intersection with an object
		Unigine.Object obj = World.GetIntersection(pos, pos + dir * maxLength, generalBitMask, intersection);
		if (obj != null)
		{
			// set the laser length
			float length = (intersection.Point - pos).Length;
			node.WorldScale = new vec3(1, length, 1);

			// show the hit point
			hitPoint.Enabled = true;
			hitPoint.WorldPosition = intersection.Point;
			hitPoint.SetWorldDirection(intersection.Normal, vec3.UP, MathLib.AXIS.Y);

			if (currentTime < 0)
			{
				// take damage
				HealthController healthController = ComponentSystem.GetComponent<HealthController>(obj);
				if (healthController != null)
				{
					healthController.TakeDamage(damage);
					currentTime = damageTimeInterval;
				}
			}
		}
		else
		{
			node.WorldScale = new vec3(1, maxLength, 1);
			hitPoint.Enabled = false;
		}

		if (currentTime >= 0)
			currentTime -= Game.IFps;
	}

	protected override void OnEnable()
	{
		// show the hit point
		if (hitPoint != null)
			hitPoint.Enabled = true;
	}

	protected override void OnDisable()
	{
		// hide the hit point
		if (hitPoint != null)
			hitPoint.Enabled = false;
	}
}
