﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "ff06f8b640a8189de61bc158dc78d4d75c5e6c4a")]
public class InitRotation : Component
{
	[ShowInEditor][Parameter(Tooltip = "Initial object orientation")]
	private vec3 initRotation = vec3.ZERO;

	[ShowInEditor][Parameter(Tooltip = "Use random orientation within the angle interval defined below")]
	bool useRandom = false;

	[ShowInEditor][Parameter(Tooltip = "Min possible rotation")]
	private vec3 minRotation = vec3.ZERO;

	[ShowInEditor][Parameter(Tooltip = "Max possible rotation")]
	private vec3 maxRotation = vec3.ZERO;

	void Init()
	{
		if (useRandom)
		{
			quat rot = new quat(Game.GetRandomFloat(minRotation.x, maxRotation.x),
									 Game.GetRandomFloat(minRotation.y, maxRotation.y),
									 Game.GetRandomFloat(minRotation.z, maxRotation.z));

			node.SetRotation(node.GetRotation() * rot);
		}
		else
			node.SetRotation(node.GetRotation() * new quat(initRotation.x, initRotation.y, initRotation.z));
	}
}
