﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "57ac715f77a38f3276ad24e9daafa934dad94303")]
public class Destroy : Component
{
	[ShowInEditor][Parameter(Tooltip = "Object's lifetime")]
	private float lifeTime = 1.0f;

	private float startTime = 0.0f;

	void Init()
	{
		// remember initialization time of an object
		startTime = Game.Time;
	}

	void Update()
	{
		// wait until the lifetime ends and delete the object
		if (Game.Time - startTime > lifeTime)
			node.DeleteLater();
	}
}
