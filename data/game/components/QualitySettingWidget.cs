﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "5b57bae1145216cc339b63bb825285decea433a5")]
public class QualitySettingWidget : Component
{
	[ShowInEditor][Parameter(Tooltip = "Items of combobox")]
	private Node itemsNode = null;

	[ShowInEditor][Parameter(Tooltip = "Default item index")]
	private int defaultItemNumber = 2;

	public struct NodeSwitcher
	{
		public Node toggableNode;
		public bool isEnabled;
	}

	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains low rendering quality settings")]
	private string lowSettingsFile = "";

	[ShowInEditor][Parameter(Tooltip = "Nodes to enable/disable with low quality preset")]
	private List<NodeSwitcher> lowSettingsNodes = null;

	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains medium rendering quality settings")]
	private string mediumSettingsFile = "";

	[ShowInEditor]
	[Parameter(Tooltip = "Nodes to enable/disable with medium quality preset")]
	private List<NodeSwitcher> mediumSettingsNodes = null;

	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains high rendering quality settings")]
	private string highSettingsFile = "";

	[ShowInEditor]
	[Parameter(Tooltip = "Nodes to enable/disable with high quality preset")]
	private List<NodeSwitcher> highSettingsNodes = null;

	[ShowInEditor][Parameter(Tooltip = "Current mode text field")]
	private ObjectText currentMode = null;

	[ShowInEditor][Parameter(Tooltip = "Low mode text field")]
	private ObjectText itemText0 = null;

	[ShowInEditor][Parameter(Tooltip = "Medium mode text field")]
	private ObjectText itemText1 = null;

	[ShowInEditor][Parameter(Tooltip = "High mode text field")]
	private ObjectText itemText2 = null;

	void Init()
	{
		// hide nodes
		itemsNode.Enabled = false;

		// set the mouse click handler for UI elements
		UiElement.onClick += OnClickHandler;

		// apply a default preset
		switch (defaultItemNumber)
		{
			case 0: OnClickHandler(UiElement.Element.Item2); break;
			case 1: OnClickHandler(UiElement.Element.Item1); break;
			case 2: OnClickHandler(UiElement.Element.Item0); break;
			default: break;
		}
	}
	
	void Update()
	{
		if (!UiElement.AnyIsSelect && Input.IsMouseButtonDown(Input.MOUSE_BUTTON.LEFT))
			itemsNode.Enabled = false;
	}

	void Shutdown()
	{
		// remove the mouse click handler for UI elements
		UiElement.onClick -= OnClickHandler;
	}

	private void OnClickHandler(UiElement.Element item)
	{
		// check the element type and show/hide nodes, apply the relevant setting
		switch (item)
		{
			case UiElement.Element.Drop:
				itemsNode.Enabled = true;
				break;

			case UiElement.Element.Item0:
				SetQuality(lowSettingsFile, lowSettingsNodes);
				currentMode.Text = itemText0.Text;
				itemsNode.Enabled = false;
				break;

			case UiElement.Element.Item1:
				SetQuality(mediumSettingsFile, mediumSettingsNodes);
				currentMode.Text = itemText1.Text;
				itemsNode.Enabled = false;
				break;

			case UiElement.Element.Item2:
				SetQuality(highSettingsFile, highSettingsNodes);
				currentMode.Text = itemText2.Text;
				itemsNode.Enabled = false;
				break;

			default: break;
		}
	}

	private void SetQuality(string file, List<NodeSwitcher> presetNodes)
	{
		// apply the preset
		if (file.Length > 0)
			Render.LoadSettings(file);

		if (presetNodes == null)
			return;

		foreach (var n in presetNodes)
			if (n.toggableNode != null && n.toggableNode.IsValidPtr)
				n.toggableNode.Enabled = n.isEnabled;
	}
}
