﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "54bbe6b8efdefab7be8598aa0ca57001bb8f096c")]
public class GameOverWidget : Component
{
	[ShowInEditor][Parameter(Tooltip = "Player's health controller")]
	private HealthController healthController = null;

	[ShowInEditor][Parameter(Tooltip = "Object with the gameover message")]
	private Node gameOverNode = null;

	void Init()
	{
		// hide the message
		gameOverNode.Enabled = false;

		// set the death event handler
		healthController.deadEvent += DeadEventHandler;

		// set the mouse click handler for UI elements (Restart/Exit)
		UiElement.onClick += OnClickHandler;
	}

	void Shutdown()
	{
		// remove handlers
		healthController.deadEvent -= DeadEventHandler;
		UiElement.onClick -= OnClickHandler;
	}

	private void DeadEventHandler()
	{
		// set gui and input
		Input.MouseHandle = Input.MOUSE_HANDLE.USER;

		// show the message
		gameOverNode.Enabled = true;
	}

	private void OnClickHandler(UiElement.Element uiType)
	{
		if (uiType == UiElement.Element.Restart)
			Unigine.Console.Run("world_reload");

		if (uiType == UiElement.Element.Exit)
			App.Exit();
	}
}
