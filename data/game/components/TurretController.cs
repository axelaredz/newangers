﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unigine;

[Component(PropertyGuid = "5de064783893dd77a6cd9c6ae78c20c10672832c")]
public class TurretController : Component
{
	[ShowInEditor][Parameter(Tooltip = "Target for turret")]
	private Node target = null;

	[ShowInEditor][ParameterSlider(Min = 0, Tooltip = "Player detection distance")]
	private float detectionDistance = 5.0f;

	[ShowInEditor][ParameterMask(Tooltip = "Bitmask for help detecting player, which contain only environment objects and player")]
	private int visibilityOfPlayer = 0B00000011;

	[ShowInEditor][ParameterMask(Tooltip = "Bitmask for detecting intersection with player")]
	private int playerMask = 0B00000010;

	[ShowInEditor][Parameter(Tooltip = "Turret fire controller")]
	private FireController fireController = null;

	[ShowInEditor][Parameter(Tooltip = "Object for animation")]
	private ObjectMeshSkinned mesh = null;

	[ShowInEditor][Parameter(Tooltip = "Turret health controller")]
	private HealthController healthController = null;

	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains explosion effect")]
	private string explosionEffectReference = null;

	private const int horizontalJointBone = 1;
	private const int verticalJointBone = 2;
	private const float interpolationFactor = 5.0f;

	private mat4 horizontalTransform;
	private mat4 verticalTransform;

	private vec3 targetPosition;

	private WorldIntersection intersection = null;

	void Init()
	{
		intersection = new WorldIntersection();

		targetPosition = target.WorldPosition;
		UpdateTransforms();

		// set the death event handler
		healthController.deadEvent += DeadEventHandler;
	}

	[MethodUpdate(Order = 1)]
	void Update()
	{
		// check the target
		if (target == null || Node.IsNode(target) == 0)
			return;

		// check the distance to the target
		if ((target.WorldPosition - node.WorldPosition).Length < detectionDistance)
		{
			// find intersection
			Unigine.Object obj = World.GetIntersection(node.WorldPosition + new vec3(0, 0, 0.5f), target.WorldPosition, visibilityOfPlayer, intersection);
			if (obj != null && obj.GetIntersectionMask(0) == playerMask)
			{
				targetPosition = MathLib.Lerp(targetPosition, target.WorldPosition, interpolationFactor * Game.IFps);

				fireController.Fire();

				UpdateTransforms();
			}
			
		}

		// update transforms of bones
		mesh.SetWorldBoneTransform(horizontalJointBone, horizontalTransform);
		mesh.SetWorldBoneChildrenTransform(verticalJointBone, verticalTransform);
	}

	void Shutdown()
	{
		// remove the death event handler
		healthController.deadEvent -= DeadEventHandler;
	}

	private void UpdateTransforms()
	{
		// set the bone transform for the horizontal joint
		horizontalTransform = mesh.GetWorldBoneTransform(horizontalJointBone);
		vec3 scale = horizontalTransform.Scale;

		horizontalTransform = MathLib.SetTo(horizontalTransform.GetColumn3(3), new vec3(targetPosition.x, targetPosition.y, horizontalTransform.GetColumn3(3).z), vec3.UP, MathLib.AXIS.Y);
		horizontalTransform *= MathLib.Scale(scale);

		// set the bone transform for the vertical joint
		verticalTransform = mesh.GetWorldBoneTransform(verticalJointBone);
		scale = verticalTransform.Scale;

		verticalTransform = MathLib.SetTo(verticalTransform.GetColumn3(3), targetPosition, vec3.UP, MathLib.AXIS.Y);
		verticalTransform *= MathLib.Scale(scale);
	}

	void DeadEventHandler()
	{
		// play the explosion effect and delete the object
		Node effect = World.LoadNode(explosionEffectReference);
		effect.WorldPosition = node.WorldPosition;

		node.DeleteLater();
	}
}
