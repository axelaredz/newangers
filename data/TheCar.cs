using System;
using System.Collections;
using System.Collections.Generic;

using System.Linq;
using System.Text;

using Unigine;

[Component(PropertyGuid = "d23188f705a451eafe445841ff6a54acd75dc81b")]
public class TheCar : Component
{

	[ShowInEditor][Parameter(Tooltip = "Left Wheel")]
	private Node targetWheelL = null;
	[ShowInEditor][Parameter(Tooltip = "Right Wheel")]
	private Node targetWheelR = null;

	[ShowInEditor][Parameter(Tooltip = "Left Wheel F")]
	private Node targetFWL = null;
	[ShowInEditor][Parameter(Tooltip = "Right Wheel F")]
	private Node targetFWR = null;

	[ShowInEditor][Parameter(Tooltip = "theCar")]
	private Node targetCar = null;

//	[ShowInEditor][Parameter(Tooltip = "theCam")]
//	private Node targetCam = null;

//    JointWheel[] wheel_joints = new JointWheel[4];
  private JointWheel my_jointL;// = new JointWheel();
  private JointWheel my_jointR;
  private JointWheel my_jointFL;
  private JointWheel my_jointFR;

	Controls controls = null;
		float angle = 0.0f;
		float velocity = 0.0f;
		float torque = 0.0f;

	private float ifps;

	[ShowInEditor][ParameterFile(Tooltip = "Asset file that contains the spawn effect")]
	private string spawnEffectReference = "";

	[ShowInEditor][ParameterFile(Tooltip = "Asset file for trace decal")]
	private string spawnTraceDecal = "";

	private float myTimer = 0.0f;

	private void Init()
	{

 	     //   // setting up physics parameters
			Physics.Gravity = new vec3(0.0f, 0.0f, -9.8f * 2.0f);
			Physics.FrozenLinearVelocity = 0.1f;
			Physics.FrozenAngularVelocity = 0.1f;

	//my_jointL= targetWheelL.GetComponent<JointWheel>();
	//my_rig= targetWheelL.GetComponent<BodyRigid>();
	my_jointL = targetWheelL.ObjectBodyRigid.GetJoint(0) as JointWheel;
	//my_jointL.AngularVelocity = 7f;
	//my_jointL.AngularTorque = 7f;

	my_jointR = targetWheelR.ObjectBodyRigid.GetJoint(0) as JointWheel;
	//my_jointR.AngularVelocity = 7f;
	//my_jointR.AngularTorque = 7f;

	my_jointFL = targetFWL.ObjectBodyRigid.GetJoint(0) as JointWheel;
	my_jointFR = targetFWR.ObjectBodyRigid.GetJoint(0) as JointWheel;

			// setting up player and controls
			PlayerPersecutor player = new PlayerPersecutor();

		//	player.Fixed = true;
		//	player.Target = targetCar;
		//	player.MinDistance = 6.0f;
		//	player.MaxDistance = 11.0f;
		//	player.Position = new vec3(0.0f, -10.0f, 6.0f);
			controls = player.Controls;
		//	player.Controlled = false;
		//	Game.Player = player;
		//	Game.Enabled = true;

	}
	
	private void Update()
	{
			ifps = Game.IFps;

           	// forward and backward movement by setting joint motor's velocity and torque
			if ((controls.GetState(Controls.STATE_FORWARD) == 1) || (controls.GetState(Controls.STATE_TURN_UP) == 1))
			{
				velocity = MathLib.Max(velocity, 0.0f);
				velocity += ifps * 50.0f;
				torque = 5.0f;
			}//Input.IsKeyDown(Input.KEY.DOWN)
			else if ((controls.GetState(Controls.STATE_BACKWARD) == 1) || (controls.GetState(Controls.STATE_TURN_DOWN) == 1))
			{
				velocity = MathLib.Min(velocity, 0.0f);
				velocity -= ifps * 50.0f;
				torque = 5.0f;
			}
			else
			{
				velocity *= MathLib.Exp(-ifps);
			}
			velocity = MathLib.Clamp(velocity, -90.0f, 90.0f);

			// steering left and right by changing Axis01 for front wheel joints
			if ((controls.GetState(Controls.STATE_MOVE_LEFT) == 1) || (controls.GetState(Controls.STATE_TURN_LEFT) == 1))
				angle += ifps * 100.0f;
			else if ((controls.GetState(Controls.STATE_MOVE_RIGHT) == 1) || (controls.GetState(Controls.STATE_TURN_RIGHT) == 1))
				angle -= ifps * 100.0f;
			else
			{
				if (MathLib.Abs(angle) < 0.25f) angle = 0.0f;
				else angle -= MathLib.Sign(angle) * ifps * 45.0f;
			}
			//angle = MathLib.Clamp(angle, -30.0f, 30.0f);//было на 30 градусов
			angle = MathLib.Clamp(angle, -11.0f, 11.0f);

			// calculating steering angles for front joints (angle_0 and angle_1)
			float base_a = 3.3f;
			float width = 3.0f;
			float angle_0 = angle;
			float angle_1 = angle;
			if (MathLib.Abs(angle) > MathLib.EPSILON)
			{
				float radius = base_a / MathLib.Tan(angle * MathLib.DEG2RAD);
				angle_0 = MathLib.Atan(base_a / (radius + width / 2.0f)) * MathLib.RAD2DEG;
				angle_1 = MathLib.Atan(base_a / (radius - width / 2.0f)) * MathLib.RAD2DEG;
			}

			my_jointFL.Axis10 = MathLib.RotateZ(angle_0).GetColumn3(0);
			my_jointFR.Axis10 = MathLib.RotateZ(angle_1).GetColumn3(0);

			// enabling or disabling a brake
			if (controls.GetState(Controls.STATE_USE) == 1)
			{
				velocity = 0.0f;
					my_jointL.AngularDamping = 20000.0f;
					my_jointR.AngularDamping = 20000.0f;
					my_jointFL.AngularDamping = 20000.0f;
					my_jointFR.AngularDamping = 20000.0f;
			}
			else
			{
					my_jointL.AngularDamping = 0.0f;
					my_jointR.AngularDamping = 0.0f;
					my_jointFL.AngularDamping = 0.0f;
					my_jointFR.AngularDamping = 0.0f;
			}

			if (Input.IsKeyDown(Input.KEY.Z))
			{
				targetCar.ObjectBodyRigid.AddLinearImpulse(vec3.UP*800f);
				//App.Exit();
			}
			if (Input.IsKeyDown(Input.KEY.X))
			{
				targetCar.ObjectBodyRigid.AddLinearImpulse(vec3.DOWN*800f);
				//App.Exit();
			}

			if (Input.IsKeyDown(Input.KEY.Q))
			{
				targetCar.ObjectBodyRigid.Frozen=false;
				//targetCar.ObjectBodyRigid.AddImpulse(targetCar.Position,vec3.RIGHT*200f);
				//impulseX = new vec3(-1f, 0f, 0f);
				//targetCar.ObjectBodyRigid.setFrozen(0);
				
				//targetCar.ObjectBodyRigid.AddLinearImpulse(vec3.RIGHT*800f);
				
				//targetCar.ObjectBodyRigid.AddLinearImpulse(targetCar.WorldRotation*800f);
				targetCar.ObjectBodyRigid.AddLinearImpulse(targetCar.GetWorldDirection(MathLib.AXIS.X) * -1200f);
			}
			if (Input.IsKeyDown(Input.KEY.E))
			{
				targetCar.ObjectBodyRigid.Frozen=false;
				targetCar.ObjectBodyRigid.AddLinearImpulse(targetCar.GetWorldDirection(MathLib.AXIS.X) * 1200f);
			}

            //string IDD1 = "less";
			//string IDD2 = "more";
			//if (velocity<=5f){Log.Message(IDD1);}else{Log.Message(IDD2);}
            //string IDD = velocity.ToString();
			//Log.Message(IDD);

            //if (velocity>0.2f||velocity<-0.2f)
			//    {
			//	//Log.Message("spawn");
			//	SledEvent();
			//	}

			// действия в таймере
			myTimer+=1f;
			
			if (myTimer>2f){
			 myTimer = 0f;

                if (velocity>0.1f||velocity<-0.1f)
			    {
				//Log.Message("spawn");
				SledEvent();
				}

			}
			// конец действий в таймере	

			//return true;
	}

		private void UpdatePhysics()
		{
			// set angular velocity for rear joints
			my_jointL.AngularVelocity = velocity;
			my_jointR.AngularVelocity = velocity;

			// set torque for rear joints
			my_jointL.AngularTorque = torque;
			my_jointR.AngularTorque = torque;

			//return true;
		}

		private void SledEvent()
	   {

	    Node spawnEffect = World.LoadNode(spawnEffectReference);
		//spawnEffect.WorldPosition = new vec3(targetWBL.GetWorldDirection(MathLib.AXIS.X));//targetWBL.WorldPosition+vec3.RIGHT*5f;
        
		//spawnEffect.WorldPosition = node.WorldPosition;//это для спавна одной
        // на частицы не сработает spawnEffect.WorldScale = new vec3(2f, 2f, 2f);
		spawnEffect.WorldPosition = targetWheelL.WorldPosition + (vec3.DOWN*0.5f);

		spawnEffect = spawnEffect.Clone();
		spawnEffect.WorldPosition = targetWheelR.WorldPosition + (vec3.DOWN*0.5f);

	    Node spawnTrace = World.LoadNode(spawnTraceDecal);
		spawnTrace.WorldPosition = node.WorldPosition;// + (vec3.DOWN*0.5f);
		spawnTrace = spawnTrace.Clone();
		spawnTrace.WorldRotate(0.0f, 0.0f, 180.0f);
		spawnTrace.WorldPosition = node.WorldPosition; // + (vec3.DOWN*0.5f);
	    }

	
}